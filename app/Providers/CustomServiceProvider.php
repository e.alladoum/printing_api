<?php

namespace App\Providers;

use App\Data\Profile;
use App\Models\User;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\ServiceProvider;

class CustomServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {

        //Define rate limiting for 60 requests in a miniute  by user id or ip
        RateLimiter::for('api', function(Request $request) {
            return Limit::perMinute(60)->by($request->user()?->id ?: $request->ip())->response(function(Request $request, array $headers) {
                return response('Les requêtes depassent le nombre défini ...', 429, $headers);
            });
        });

        Gate::define('isAdministrator', function (User $user) {
            return $user->profile == Profile::ADMIN;
        });

        Gate::define('isAgent', function (User $user) {
            return $user->profile != Profile::ADMIN;
        });

        JsonResource::withoutWrapping();
    }
}
