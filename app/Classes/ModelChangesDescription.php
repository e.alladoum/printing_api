<?php

namespace App\Classes;
use Illuminate\Database\Eloquent\Model;
use Stringable;

class ModelChangesDescription implements Stringable
{
    protected Model $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function __toString()
    {
        $changes = $this->model->getChanges();

        if (empty($changes)) {
            return 'No changes detected.';
        }

        $description = '';

        foreach ($changes as $attribute => $newValue) {
            $originalValue = $this->model->getOriginal($attribute);

            $description .= "- $attribute: changed from '$originalValue' to '$newValue'.\n";
        }

        return $description;
    }

    public function toString(): string
    {
        return $this->__toString();
    }
}
