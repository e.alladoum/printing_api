<?php

namespace App\Classes;

use App\Models\User;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Carbon;

class Trace implements Arrayable
{
    public function __construct(
        private User   $user,
        private string $action,
        private string $description,
        private string $usedModule
    ) {
    }

    public function toArray(): array
    {
        return [
            "who" => $this->user,
            "where" => $this->usedModule,
            "what" => [
                'action' => $this->action,
                'description' => $this->description
            ],
            "when" => Carbon::now()->format(Constant::DATE_FORMAT_YMD_HIS),
        ];
    }
}
