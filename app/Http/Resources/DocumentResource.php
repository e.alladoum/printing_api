<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class DocumentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'uuid' => $this->uuid,
            'number' => $this->number,
            'files' => $this->files,
            'document_type_id' => $this->whenNotNull($this->document_type_id),
            'status' => $this->status,
            'status_label' => $this->status_label,
            'stacktrace' => $this->stacktrace,
            'batch_id' => $this->whenNotNull($this->batch_id),
            'document_type' => new DocumentTypeResource($this->whenLoaded('documentType')),
            'batch' => new BatchResource($this->whenLoaded('batch')),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ];
    }
}
