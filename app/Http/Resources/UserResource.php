<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'profile' => $this->profile,
            'registration' => $this->registration,
            'email' => $this->email,
            'name' => $this->name,
            'firstname' => $this->firstname,
            'profile_label' => $this->profile_label,
            'password' => $this->password,
            'image' => $this->image,
            'is_locked' => $this->is_locked,
            'has_password_temp' => $this->has_password_temp,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ];
    }
}
