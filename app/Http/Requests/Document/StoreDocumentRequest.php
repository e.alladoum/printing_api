<?php

namespace App\Http\Requests\Document;

use App\Models\Document;
use Illuminate\Foundation\Http\FormRequest;

class StoreDocumentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return $this->user()->can('create', Document::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'number' => 'required|string|unique:documents,number',
            'document_type_id' => 'required|integer',
            'status' => 'nullable|string',
            'batch_id' => 'nullable',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'number.required' => 'Le nombre est obligatoire',
            'number.string' => "Le nombre  n'est pas valide",
            'number.unique' => "Le nombre doit être unique",
            'document_type_id.required' => 'Le type de document est obligatoire',
            'document_type_id.integer' => 'Le type de document doit être un entier',
            'status.string' => 'Le status est une chaîne de caractères',
        ];
    }
}
