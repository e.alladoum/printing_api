<?php

namespace App\Http\Requests\User;

use App\Rules\AlphaWithSpaces;
use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return $this->user()->can('update', $this->user);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'registration' => 'required|string',
            'name' => ['required', new AlphaWithSpaces],
            'firstname' => ['required', new AlphaWithSpaces],
            'email' => 'required|email',
            'profile' => 'required|string',
            'is_locked' => 'nullable',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'registration.required' => 'La régistration est obligatoire',
            'registration.string' => "La régistration  n'est pas valide",
            'name.required' => 'Le nom est obligatoire',
            'firstname.required' => 'Le prénom est obligatoire',
            'email.required' => "L'adresse mail est obligatoire",
            'email.email' => "Le format de l'adresse mail n'est pas correcte",
            'profile.required' => 'Le profil est obligatoire',
        ];
    }
}
