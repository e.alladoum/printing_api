<?php

namespace App\Http\Requests\Batch;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBatchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return $this->user()->can('update', $this->batch);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'label' => 'nullable|string',
            'number' => 'required|integer',
            'begin' => 'required|date_format:Y-m-d H:i:s',
            'end' => 'required|date_format:Y-m-d H:i:s|after:begin',
            'documents' => 'required|array'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        $requiredMessage = '%s est obligatoire';

        return [
            'label.string' =>"Le libellé est une chaîne de caractères",
            'number.required' => sprintf($requiredMessage, 'Le nombre'),
            'number.integer' => 'Le nombre doit être un entier',
            'begin.required' => sprintf($requiredMessage, 'La date du début'),
            'begin.date_format' => 'Le champ doit être au format Y-m-d H:i:s',
            'end.required' => sprintf($requiredMessage, 'La date de la fin'),
            'end.date_format' => 'Le champ doit être au format Y-m-d H:i:s',
            'end.after' => 'La date de fin doit être supérieure à la date du début',
            'documents.required' => sprintf($requiredMessage, 'La liste des documents ids'),
            'documents.array' => 'La liste des documents doit être un tableau',
        ];
    }
}
