<?php

namespace App\Http\Requests\DocumentType;

use Illuminate\Foundation\Http\FormRequest;

class UpdateDocumentTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return $this->user()->can('update', $this->document_type);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'description' => 'nullable|string',
            'configuration' => 'nullable|string'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        $requiredMessage = '%s est obligatoire';
        $stringMessage = '%s doit être une chaîne des caractères';

        return [
            'name.required' => sprintf($requiredMessage, 'Le nom'),
            'name.string' => sprintf($stringMessage, 'Le nom'),
            'description.string' => sprintf($stringMessage, 'La description'),
            'configuration.string' => sprintf($stringMessage, 'La configuration'),
        ];
    }
}
