<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\ModelException;
use App\Http\Controllers\Controller;
use App\Models\Batch;
use App\Http\Requests\Batch\StoreBatchRequest;
use App\Http\Requests\Batch\UpdateBatchRequest;
use App\Http\Resources\BatchResource;
use App\Services\Domain\BatchService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;

class BatchController extends Controller
{

    public function __construct(private BatchService $batchService)
    {
        
    }
    /**
     * Display a listing of the resource.
     *
     * @OA\Get(
     *       path="/api/v1/auth/batches",
     *       operationId="batches",
     *       tags={"Batches"},
     *       summary="Get list of batches",
     *       description="Order Batches in descending order of ID",
     *       security={
     *           {"sanctumToken": {}}
     *       },
     *
     *        @OA\Parameter(
     *             name="updated_at",
     *             in="query",
     *             description="Filtre the batches by updated_at",
     *             required=false,
     *
     *             @OA\Schema(
     *                 type="date"
     *             )
     *         ),
     *        @OA\Parameter(
     *             name="documents",
     *             in="query",
     *             description="Load the relationship documents",
     *             required=false,
     *         ),
     *
     *       @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *
     *          @OA\JsonContent(
     *              type="array",
     *
     *              @OA\Items(ref="#/components/schemas/BatchOutput")
     *          )
     *       ),
     *
     *       @OA\Response(
     *          response=400,
     *          description="Something went wrong"
     *       ),
     *    )
     */
    public function index(Request $request)
    {
        Gate::authorize('viewAny', Batch::class);
        $batches = $this->batchService->getQuery($request);

        return BatchResource::collection($batches->get());
    }

   /**
     * Store a newly created resource in storage.
     *
     * @OA\Post(
     *       path="/api/v1/auth/batches",
     *       operationId="storeBatch",
     *       tags={"Batches"},
     *       summary="Store Batch",
     *       description="Store Batch",
     *       security={
     *           {"sanctumToken": {}}
     *       },
     *
     *        @OA\RequestBody(
     *            required=true,
     *            @OA\JsonContent(ref="#/components/schemas/BatchInput")
     *       ),
     *
     *       @OA\Response(
     *           response=200,
     *           description="Success",
     *
     *          @OA\JsonContent(ref="#/components/schemas/BatchOutput")
     *       ),
     *
     *       @OA\Response(
     *           response=400,
     *           description="Bad request"
     *       ),
     *       @OA\Response(
     *           response=401,
     *           description="Unauthorized"
     *       ),
     *       @OA\Response(
     *           response=403,
     *           description="Forbidden"
     *       ),
     *       @OA\Response(
     *           response=404,
     *           description="Not found"
     *       ),
     *       @OA\Response(
     *           response=422,
     *           description="Unprocessable Entity"
     *       )
     *  )
     */
    public function store(StoreBatchRequest $request)
    {
        $batch = $this->batchService->setBatch(new Batch(), $request);
        return (new BatchResource($batch))->response()->setStatusCode(Response::HTTP_CREATED);
    }

   /**
     * Get one the resource.
     *
     * @OA\Get(
     *      path="/api/v1/auth/batches/{id}",
     *      operationId="batchShow",
     *      tags={"Batches"},
     *      summary="Get one Batch",
     *      description="Get Batch by id",
     *      security={
     *          {"sanctumToken": {}}
     *      },
     *
     *       @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The ID of the batch to show",
     *         required=true,
     *
     *         @OA\Schema(
     *           example=1,
     *           type="number"
     *         )
     *       ),
     *
     *      @OA\Response(
     *          response="200",
     *          description="show batch",
     *
     *          @OA\JsonContent(ref="#/components/schemas/BatchOutput")
     *      ),
     *
     *      @OA\Response(
     *         response=400,
     *         description="Something went wrong"
     *      ),
     *   )
     */
    public function show(Batch $batch)
    {
        try {
            $this->throwModelNotFoundException($batch);

            return new BatchResource($batch);
        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'success' => false,
            ], ($e instanceof ModelException) ? $e->getStatusCode() : Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update a old resource in storage.
     *
     * @OA\PUT(
     *       path="/api/v1/auth/batches/{id}",
     *       operationId="updateBatch",
     *       tags={"Batches"},
     *       summary="Update Batch",
     *       description="Update Batch",
     *       security={
     *           {"sanctumToken": {}}
     *       },
     *       @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The ID of the batch to update",
     *         required=true,
     *
     *         @OA\Schema(
     *           example=1,
     *           type="number"
     *         )
     *       ),
     *        @OA\RequestBody(
     *            required=true,
     *            @OA\JsonContent(ref="#/components/schemas/BatchInput")
     *       ),
     *
     *       @OA\Response(
     *           response=200,
     *           description="Success",
     *
     *          @OA\JsonContent(ref="#/components/schemas/BatchOutput")
     *       ),
     *
     *       @OA\Response(
     *           response=400,
     *           description="Bad request"
     *       ),
     *       @OA\Response(
     *           response=401,
     *           description="Unauthorized"
     *       ),
     *       @OA\Response(
     *           response=403,
     *           description="Forbidden"
     *       ),
     *       @OA\Response(
     *           response=404,
     *           description="Not found"
     *       ),
     *       @OA\Response(
     *           response=422,
     *           description="Unprocessable Entity"
     *       )
     *  )
     */
    public function update(UpdateBatchRequest $request, Batch $batch)
    {
        try {
            $this->throwModelNotFoundException($batch);
            $batch = $this->batchService->setBatch($batch, $request);

            return (new BatchResource($batch))->response()->setStatusCode(Response::HTTP_ACCEPTED);
        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'success' => false,
            ], ($e instanceof ModelException) ? $e->getStatusCode() : Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Delete a resource in storage.
     *
     * @OA\DELETE(
     *       path="/api/v1/auth/batches/{id}",
     *       operationId="deleteBatch",
     *       tags={"Batches"},
     *       summary="Delete Batch",
     *       description="Delete Batch",
     *       security={
     *           {"sanctumToken": {}}
     *       },
     *       @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The ID of the batch to delete",
     *         required=true,
     *
     *         @OA\Schema(
     *           example=1,
     *           type="number"
     *         )
     *       ),
     *       @OA\Response(
     *           response=200,
     *           description="Success",
     *       ),
     *
     *       @OA\Response(
     *           response=400,
     *           description="Bad request"
     *       ),
     *       @OA\Response(
     *           response=401,
     *           description="Unauthorized"
     *       ),
     *       @OA\Response(
     *           response=403,
     *           description="Forbidden"
     *       ),
     *       @OA\Response(
     *           response=404,
     *           description="Not found"
     *       ),
     *       @OA\Response(
     *           response=422,
     *           description="Unprocessable Entity"
     *       )
     *  )
     */
    public function destroy(Batch $batch)
    {
        try {
            $this->throwModelNotFoundException($batch);
            $this->batchService->deleteBatch($batch);
            return response(null, Response::HTTP_NO_CONTENT);
        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'success' => false,
            ], ($e instanceof ModelException) ? $e->getStatusCode() : Response::HTTP_BAD_REQUEST);
        }
    }
}
