<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\LoginUserRequest;
use App\Services\SecurityService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SecurityController extends Controller
{
    public function __construct(private SecurityService $securityService)
    {
    }

    /**
     * Login
     * @OA\Post(
     *     path="/api/v1/login",
     *     tags={"Authentication"},
     *     summary="Login",
     *     description="Returns response",
     *
     *     @OA\RequestBody(
     *         required=true,
     *         description="Login credentials",
     *
     *         @OA\JsonContent(
     *             required={"registration", "password"},
     *
     *             @OA\Property(property="registration", type="string", format="registration", example="john@example.com"),
     *             @OA\Property(property="password", type="string", format="password", example="password123")
     *         ),
     *     ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", format="message", example="Connexion réussie"),
     *              @OA\Property(property="success", type="boolean", format="succes", example="true"),
     *              @OA\Property(property="user", ref="#/components/schemas/UserOutput"),
     *              @OA\Property(property="token", type="string", format="JWT", example="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c")
     *          )
     *      ),
     *
     *       @OA\Response(
     *           response=403,
     *           description="Failure operation",
     *
     *           @OA\JsonContent(
     *               @OA\Property(property="message", type="string", format="message", example="Échec de la connexion, réessayez plustard."),
     *               @OA\Property(property="success", type="boolean", format="succes", example="false"),
     *               @OA\Property(property="user", type="object", format="user", example="null")
     *           )
     *       ),
     *
     *        @OA\Response(
     *            response=500,
     *            description="Failure operation",
     *
     *            @OA\JsonContent(
     *
     *                @OA\Property(property="message", type="string", format="message", example="Impossible de créer le jeton"),
     *                @OA\Property(property="success", type="boolean", format="succes", example="false"),
     *                 @OA\Property(property="error", type="string", format="error", example="something went wrong")
     *            )
     *        ),
     *
     *        @OA\Response(
     *            response=400,
     *            description="Failure operation",
     *
     *            @OA\JsonContent(
     *
     *                @OA\Property(property="message", type="string", format="message", example="Échec de connexion, vérifiez vos données et envoyez à nouveau"),
     *                @OA\Property(property="success", type="boolean", format="succes", example="false"),
     *            )
     *        ),
     *
     *     @OA\Response(
     *         response=401,
     *         description="Invalid credentials"
     *     )
     * )
     */
    public function login(LoginUserRequest $request): JsonResponse
    {
        return $this->securityService->loginApi($request);
    }


    /**
     * Logout
     * @OA\Post (
     *     path="/api/v1/auth/logout",
     *     tags={"Authentication"},
     *     security={ {"sanctumToken": {}}},
     *     @OA\Response(
     *         response=200,
     *         description="Success operation",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", format="message", example="Utilisateur déconnecté.")
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=401,
     *         description="Unauthenticated",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", format="message", example="Unauthenticated")
     *         )
     *     )
     * )
     */
    public function logout(Request $request): JsonResponse
    {
        return $this->securityService->logoutApi($request);
    }
}
