<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\ModelException;
use App\Http\Controllers\Controller;
use App\Models\DocumentType;
use App\Http\Requests\DocumentType\StoreDocumentTypeRequest;
use App\Http\Requests\DocumentType\UpdateDocumentTypeRequest;
use App\Http\Resources\DocumentTypeResource;
use App\Services\Domain\DocumentTypeService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;

class DocumentTypeController extends Controller
{

    public function __construct(private DocumentTypeService $documentTypeService)
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @OA\Get(
     *       path="/api/v1/auth/admin/document_types",
     *       operationId="documents_types",
     *       tags={"Document Types"},
     *       summary="Get list of documents types",
     *       description="Order Document Types in descending order of ID",
     *       security={
     *           {"sanctumToken": {}}
     *       },
     *
     *        @OA\Parameter(
     *             name="updated_at",
     *             in="query",
     *             description="Filtre the documents types by updated_at",
     *             required=false,
     *
     *             @OA\Schema(
     *                 type="date"
     *             )
     *         ),
     *
     *       @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *
     *          @OA\JsonContent(
     *              type="array",
     *
     *              @OA\Items(ref="#/components/schemas/DocumentTypeOutput")
     *          )
     *       ),
     *
     *       @OA\Response(
     *          response=400,
     *          description="Something went wrong"
     *       ),
     *    )
     */
    public function index(Request $request)
    {
        Gate::authorize('viewAny', DocumentType::class);
        $documentTypes = Cache::remember('documentTypes', 60, function() use ($request) {
            return $this->documentTypeService->getQuery($request)->get();
        });

        return DocumentTypeResource::collection($documentTypes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @OA\Post(
     *       path="/api/v1/auth/admin/document_types",
     *       operationId="storeDocumentType",
     *       tags={"Document Types"},
     *       summary="Store Document Type",
     *       description="Store Document Type",
     *       security={
     *           {"sanctumToken": {}}
     *       },
     *
     *        @OA\RequestBody(
     *            required=true,
     *            @OA\JsonContent(ref="#/components/schemas/DocumentTypeInput")
     *       ),
     *
     *       @OA\Response(
     *           response=200,
     *           description="Success",
     *
     *          @OA\JsonContent(ref="#/components/schemas/DocumentTypeOutput")
     *       ),
     *
     *       @OA\Response(
     *           response=400,
     *           description="Bad request"
     *       ),
     *       @OA\Response(
     *           response=401,
     *           description="Unauthorized"
     *       ),
     *       @OA\Response(
     *           response=403,
     *           description="Forbidden"
     *       ),
     *       @OA\Response(
     *           response=404,
     *           description="Not found"
     *       ),
     *       @OA\Response(
     *           response=422,
     *           description="Unprocessable Entity"
     *       )
     *  )
     */
    public function store(StoreDocumentTypeRequest $request)
    {
        $documentType = $this->documentTypeService->setDocumentType(new DocumentType(), $request);
        return (new DocumentTypeResource($documentType))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Get one the resource.
     *
     * @OA\Get(
     *      path="/api/v1/auth/admin/document_types/{id}",
     *      operationId="documenttypeShow",
     *      tags={"Document Types"},
     *      summary="Get one Document Type",
     *      description="Get Document Type by id",
     *      security={
     *          {"sanctumToken": {}}
     *      },
     *
     *       @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The ID of the document type to show",
     *         required=true,
     *
     *         @OA\Schema(
     *           example=1,
     *           type="number"
     *         )
     *       ),
     *
     *      @OA\Response(
     *          response="200",
     *          description="show document type",
     *
     *          @OA\JsonContent(ref="#/components/schemas/DocumentTypeOutput")
     *      ),
     *
     *      @OA\Response(
     *         response=400,
     *         description="Something went wrong"
     *      ),
     *   )
     */
    public function show(DocumentType $documentType)
    {
        Gate::authorize('view', $documentType);
        try {
            $this->throwModelNotFoundException($documentType);

            return new DocumentTypeResource($documentType);
        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'success' => false,
            ], ($e instanceof ModelException) ? $e->getStatusCode() : Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update a old resource in storage.
     *
     * @OA\PUT(
     *       path="/api/v1/auth/admin/document_types/{id}",
     *       operationId="updateDocumentType",
     *       tags={"Document Types"},
     *       summary="Update Document Type",
     *       description="Update Document Type",
     *       security={
     *           {"sanctumToken": {}}
     *       },
     *       @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The ID of the document type to update",
     *         required=true,
     *
     *         @OA\Schema(
     *           example=1,
     *           type="number"
     *         )
     *       ),
     *        @OA\RequestBody(
     *            required=true,
     *            @OA\JsonContent(ref="#/components/schemas/DocumentTypeInput")
     *       ),
     *
     *       @OA\Response(
     *           response=200,
     *           description="Success",
     *
     *          @OA\JsonContent(ref="#/components/schemas/DocumentTypeOutput")
     *       ),
     *
     *       @OA\Response(
     *           response=400,
     *           description="Bad request"
     *       ),
     *       @OA\Response(
     *           response=401,
     *           description="Unauthorized"
     *       ),
     *       @OA\Response(
     *           response=403,
     *           description="Forbidden"
     *       ),
     *       @OA\Response(
     *           response=404,
     *           description="Not found"
     *       ),
     *       @OA\Response(
     *           response=422,
     *           description="Unprocessable Entity"
     *       )
     *  )
     */
    public function update(UpdateDocumentTypeRequest $request, DocumentType $documentType)
    {
        DB::beginTransaction();
        try {
            $this->throwModelNotFoundException($documentType);
            $documentType = $this->documentTypeService->setDocumentType($documentType, $request);
            DB::commit();
            return (new DocumentTypeResource($documentType))->response()->setStatusCode(Response::HTTP_ACCEPTED);
        } catch (Exception $e) {

            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
                'success' => false,
            ], ($e instanceof ModelException) ? $e->getStatusCode() : Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Delete a resource in storage.
     *
     * @OA\DELETE(
     *       path="/api/v1/auth/admin/document_types/{id}",
     *       operationId="deleteDocumentType",
     *       tags={"Document Types"},
     *       summary="Delete Document Type",
     *       description="Delete Document Type",
     *       security={
     *           {"sanctumToken": {}}
     *       },
     *       @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The ID of the document type to delete",
     *         required=true,
     *
     *         @OA\Schema(
     *           example=1,
     *           type="number"
     *         )
     *       ),
     *       @OA\Response(
     *           response=200,
     *           description="Success",
     *       ),
     *
     *       @OA\Response(
     *           response=400,
     *           description="Bad request"
     *       ),
     *       @OA\Response(
     *           response=401,
     *           description="Unauthorized"
     *       ),
     *       @OA\Response(
     *           response=403,
     *           description="Forbidden"
     *       ),
     *       @OA\Response(
     *           response=404,
     *           description="Not found"
     *       ),
     *       @OA\Response(
     *           response=422,
     *           description="Unprocessable Entity"
     *       )
     *  )
     */
    public function destroy(DocumentType $documentType)
    {
        Gate::authorize('delete', $documentType);

        try {
            $this->throwModelNotFoundException($documentType);
            $this->documentTypeService->deleteDocumentType($documentType);
            return response(null, Response::HTTP_NO_CONTENT);
        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'success' => false,
            ], ($e instanceof ModelException) ? $e->getStatusCode() : Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display a documents count by type.
     *
     * @OA\Get(
     *       path="/api/v1/auth/admin/document_types/documents/count",
     *       operationId="documents_types_count",
     *       tags={"Document Types"},
     *       summary="Get documents count by type",
     *       description="documents count by type",
     *       security={
     *           {"sanctumToken": {}}
     *       },
     *
     *        @OA\Parameter(
     *             name="document_type_name",
     *             in="query",
     *             description="Filtre the documents count by type name",
     *             required=false,
     *
     *             @OA\Schema(
     *                 type="string"
     *             )
     *         ),
     *
     *       @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *                  @OA\Property(property="id", type="number", example=1),
     *                  @OA\Property(property="name", type="string", example="Permis"),
     *                  @OA\Property(property="description", type="string", example=null),
     *                  @OA\Property(property="configuration", type="string", example=null),
     *                  @OA\Property(property="created_at", type="date", example=null),
     *                  @OA\Property(property="updated_at", type="date", example=null),
     *                  @OA\Property(property="documents_all", type="number", example=0),
     *                  @OA\Property(property="documents_printed", type="number", example=0),
     *                  @OA\Property(property="documents_pending", type="number", example=0),
     *          )
     *       ),
     *       @OA\Response(
     *          response=400,
     *          description="Something went wrong"
     *       ),
     *    )
     */
    public function count(Request $request)
    {
        $result = $this->documentTypeService->count($request);
        return response()->json($result);
    }
}
