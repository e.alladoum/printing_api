<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\ModelException;
use App\Http\Controllers\Controller;
use App\Models\Document;
use App\Http\Requests\Document\StoreDocumentRequest;
use App\Http\Requests\Document\UpdateDocumentRequest;
use App\Http\Resources\DocumentResource;
use App\Services\Domain\DocumentService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;

class DocumentController extends Controller
{

    public function __construct(private DocumentService $documentService)
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @OA\Get(
     *       path="/api/v1/auth/documents",
     *       operationId="documents",
     *       tags={"Documents"},
     *       summary="Get list of documents",
     *       description="Order Documents in descending order of ID",
     *       security={
     *           {"sanctumToken": {}}
     *       },
     *
     *        @OA\Parameter(
     *             name="updated_at",
     *             in="query",
     *             description="Filtre the documents by updated_at",
     *             required=false,
     *
     *             @OA\Schema(
     *                 type="date"
     *             )
     *         ),
     *        @OA\Parameter(
     *             name="batch_id",
     *             in="query",
     *             description="Filter the  by batch_id",
     *             required=false,
     *
     *             @OA\Schema(
     *                 type="number",
     *             )
     *         ),
     * 
     *        @OA\Parameter(
     *             name="document_type_id",
     *             in="query",
     *             description="Filter the documents by document_type_id",
     *             required=false,
     *
     *             @OA\Schema(
     *                 type="number",
     *             )
     *         ),
     * 
     *        @OA\Parameter(
     *             name="status",
     *             in="query",
     *             description="Filter the documents by status",
     *             required=false,
     *
     *             @OA\Schema(
     *                 type="string",
     *             )
     *         ),
     * 
     *        @OA\Parameter(
     *             name="document_type",
     *             in="query",
     *             description="Load the relationship document_type",
     *             required=false,
     *         ),
     *        @OA\Parameter(
     *             name="batch",
     *             in="query",
     *             description="Load the relationship batch",
     *             required=false,
     *         ),
     *
     *       @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *
     *          @OA\JsonContent(
     *              type="array",
     *
     *              @OA\Items(ref="#/components/schemas/DocumentOuput")
     *          )
     *       ),
     *
     *       @OA\Response(
     *          response=400,
     *          description="Something went wrong"
     *       ),
     *    )
     */
    public function index(Request $request)
    {
        Gate::authorize('viewAny', Document::class);
        $documents = $this->documentService->getQuery($request);

        return DocumentResource::collection($documents->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @OA\Post(
     *       path="/api/v1/auth/documents",
     *       operationId="storeDocument",
     *       tags={"Documents"},
     *       summary="Store Document",
     *       description="Store Document",
     *       security={
     *           {"sanctumToken": {}}
     *       },
     *
     *        @OA\RequestBody(
     *            required=true,
     *            @OA\JsonContent(ref="#/components/schemas/DocumentInput")
     *       ),
     *
     *       @OA\Response(
     *           response=200,
     *           description="Success",
     *
     *          @OA\JsonContent(ref="#/components/schemas/DocumentOuput")
     *       ),
     *
     *       @OA\Response(
     *           response=400,
     *           description="Bad request"
     *       ),
     *       @OA\Response(
     *           response=401,
     *           description="Unauthorized"
     *       ),
     *       @OA\Response(
     *           response=403,
     *           description="Forbidden"
     *       ),
     *       @OA\Response(
     *           response=404,
     *           description="Not found"
     *       ),
     *       @OA\Response(
     *           response=422,
     *           description="Unprocessable Entity"
     *       )
     *  )
     */
    public function store(StoreDocumentRequest $request)
    {
        $document = $this->documentService->setDocument(new Document(), $request);
        return (new DocumentResource($document))->response()->setStatusCode(Response::HTTP_CREATED);
    }

   /**
     * Get one the resource.
     *
     * @OA\Get(
     *      path="/api/v1/auth/documents/{id}",
     *      operationId="documentShow",
     *      tags={"Documents"},
     *      summary="Get one Document",
     *      description="Get Document by id",
     *      security={
     *          {"sanctumToken": {}}
     *      },
     *
     *       @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The ID of the document to show",
     *         required=true,
     *
     *         @OA\Schema(
     *           example=1,
     *           type="number"
     *         )
     *       ),
     *
     *      @OA\Response(
     *          response="200",
     *          description="show document type",
     *
     *          @OA\JsonContent(ref="#/components/schemas/DocumentOuput")
     *      ),
     *
     *      @OA\Response(
     *         response=400,
     *         description="Something went wrong"
     *      ),
     *   )
     */
    public function show(Document $document)
    {
        Gate::authorize('view', $document);

        try {
            $this->throwModelNotFoundException($document);

            return new DocumentResource($document);
        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'success' => false,
            ], ($e instanceof ModelException) ? $e->getStatusCode() : Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update a old resource in storage.
     *
     * @OA\PUT(
     *       path="/api/v1/auth/documents/{id}",
     *       operationId="updateDocument",
     *       tags={"Documents"},
     *       summary="Update Document",
     *       description="Update Document",
     *       security={
     *           {"sanctumToken": {}}
     *       },
     *       @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The ID of the document to update",
     *         required=true,
     *
     *         @OA\Schema(
     *           example=1,
     *           type="number"
     *         )
     *       ),
     *        @OA\RequestBody(
     *            required=true,
     *            @OA\JsonContent(ref="#/components/schemas/DocumentInput")
     *       ),
     *
     *       @OA\Response(
     *           response=200,
     *           description="Success",
     *
     *          @OA\JsonContent(ref="#/components/schemas/DocumentOuput")
     *       ),
     *
     *       @OA\Response(
     *           response=400,
     *           description="Bad request"
     *       ),
     *       @OA\Response(
     *           response=401,
     *           description="Unauthorized"
     *       ),
     *       @OA\Response(
     *           response=403,
     *           description="Forbidden"
     *       ),
     *       @OA\Response(
     *           response=404,
     *           description="Not found"
     *       ),
     *       @OA\Response(
     *           response=422,
     *           description="Unprocessable Entity"
     *       )
     *  )
     */
    public function update(UpdateDocumentRequest $request, Document $document)
    {
        try {
            $this->throwModelNotFoundException($document);
            $document = $this->documentService->setDocument($document, $request);

            return (new DocumentResource($document))->response()->setStatusCode(Response::HTTP_ACCEPTED);
        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'success' => false,
            ], ($e instanceof ModelException) ? $e->getStatusCode() : Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Delete a resource in storage.
     *
     * @OA\DELETE(
     *       path="/api/v1/auth/documents/{id}",
     *       operationId="deleteDocument",
     *       tags={"Documents"},
     *       summary="Delete Document",
     *       description="Delete Document",
     *       security={
     *           {"sanctumToken": {}}
     *       },
     *       @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The ID of the document to delete",
     *         required=true,
     *
     *         @OA\Schema(
     *           example=1,
     *           type="number"
     *         )
     *       ),
     *       @OA\Response(
     *           response=200,
     *           description="Success",
     *       ),
     *
     *       @OA\Response(
     *           response=400,
     *           description="Bad request"
     *       ),
     *       @OA\Response(
     *           response=401,
     *           description="Unauthorized"
     *       ),
     *       @OA\Response(
     *           response=403,
     *           description="Forbidden"
     *       ),
     *       @OA\Response(
     *           response=404,
     *           description="Not found"
     *       ),
     *       @OA\Response(
     *           response=422,
     *           description="Unprocessable Entity"
     *       )
     *  )
     */
    public function destroy(Document $document)
    {
        Gate::authorize('view', $document);
        try {
            $this->throwModelNotFoundException($document);
            $this->documentService->deleteDocument($document);
            return response(null, Response::HTTP_NO_CONTENT);
        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'success' => false,
            ], ($e instanceof ModelException) ? $e->getStatusCode() : Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * ChangeStatus
     *
     * @OA\POST(
     *       path="/api/v1/auth/documents/{id}/changeStatus",
     *       operationId="changeStatus",
     *       tags={"Documents"},
     *       summary="changeStatus",
     *       description="changeStatus",
     *       security={
     *           {"sanctumToken": {}}
     *       },
     *       @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The ID of the document to change status",
     *         required=true,
     *
     *         @OA\Schema(
     *           example=1,
     *           type="number"
     *         )
     *       ),
     *       @OA\Parameter(
     *         name="state",
     *         in="path",
     *         description="The status of the document to change",
     *         required=true,
     *
     *         @OA\Schema(
     *           example="pending",
     *           type="string"
     *         )
     *       ),
     *       @OA\Response(
     *           response=200,
     *           description="Success",
     *           @OA\JsonContent(ref="#/components/schemas/DocumentOuput")
     *       ),
     *
     *       @OA\Response(
     *           response=400,
     *           description="Bad request"
     *       ),
     *       @OA\Response(
     *           response=401,
     *           description="Unauthorized"
     *       ),
     *       @OA\Response(
     *           response=403,
     *           description="Forbidden"
     *       ),
     *       @OA\Response(
     *           response=404,
     *           description="Not found"
     *       ),
     *       @OA\Response(
     *           response=422,
     *           description="Unprocessable Entity"
     *       )
     *  )
     */
    public function changeStatus(Request $request, Document $document)
    {
        Gate::authorize('status', $document);

        try {
            $this->throwModelNotFoundException($document);
            $document = $this->documentService->changeDocumentStatus($document, $request);
            return new DocumentResource($document);
        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'success' => false,
            ], ($e instanceof ModelException) ? $e->getStatusCode() : Response::HTTP_BAD_REQUEST);
        }
    }
}
