<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\ModelException;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Http\Requests\User\StoreUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Resources\UserResource;
use App\Services\Domain\UserService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{

    public function __construct(private UserService $userService)
    {
    }


    /**
     * Display a listing of the resource.
     *
     * @OA\Get(
     *       path="/api/v1/auth/admin/users",
     *       operationId="users",
     *       tags={"Users"},
     *       summary="Get list of users",
     *       description="Order users in descending order of ID",
     *       security={
     *           {"sanctumToken": {}}
     *       },
     *
     *        @OA\Parameter(
     *             name="updated_at",
     *             in="query",
     *             description="Filter users by updated_at",
     *             required=false,
     *
     *             @OA\Schema(
     *                 type="date"
     *             )
     *         ),
     *
     *       @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *
     *          @OA\JsonContent(
     *              type="array",
     *
     *              @OA\Items(ref="#/components/schemas/UserOutput")
     *          )
     *       ),
     *
     *       @OA\Response(
     *          response=400,
     *          description="Something went wrong"
     *       ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *       ),
     *    )
     */
    public function index(Request $request)
    {
        Gate::authorize('viewAny', User::class);
        $users = $this->userService->getQuery($request);

        return UserResource::collection($users->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @OA\Post(
     *       path="/api/v1/auth/admin/users",
     *       operationId="storeUser",
     *       tags={"Users"},
     *       summary="Store User",
     *       description="Store User",
     *       security={
     *           {"sanctumToken": {}}
     *       },
     *
     *        @OA\RequestBody(
     *            required=true,
     *            @OA\JsonContent(ref="#/components/schemas/UserInput"),
     *            @OA\MediaType(
     *                mediaType="multipart/form-data",
     *
     *                @OA\Schema(
     *                    type="object",
     *                    ref="#/components/schemas/UserInput")
     *            ),
     *       ),
     *
     *       @OA\Response(
     *           response=200,
     *           description="Success",
     *
     *          @OA\JsonContent(ref="#/components/schemas/UserOutput")
     *       ),
     *
     *       @OA\Response(
     *           response=400,
     *           description="Bad request"
     *       ),
     *       @OA\Response(
     *           response=401,
     *           description="Unauthorized"
     *       ),
     *       @OA\Response(
     *           response=403,
     *           description="Forbidden"
     *       ),
     *       @OA\Response(
     *           response=404,
     *           description="Not found"
     *       ),
     *       @OA\Response(
     *           response=422,
     *           description="Unprocessable Entity"
     *       )
     *  )
     */
    public function store(StoreUserRequest $request)
    {
        $user = $this->userService->setUser(new User(), $request);
        return (new UserResource($user))->response()->setStatusCode(Response::HTTP_CREATED);
    }


    /**
     * Get one the resource.
     *
     * @OA\Get(
     *      path="/api/v1/auth/admin/users/{id}",
     *      operationId="userShow",
     *      tags={"Users"},
     *      summary="Get one User",
     *      description="Get User by id",
     *      security={
     *          {"sanctumToken": {}}
     *      },
     *
     *       @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The ID of the User to show",
     *         required=true,
     *
     *         @OA\Schema(
     *           example=1,
     *           type="number"
     *         )
     *       ),
     *
     *      @OA\Response(
     *          response="200",
     *          description="show User",
     *
     *          @OA\JsonContent(ref="#/components/schemas/UserOutput")
     *      ),
     *
     *      @OA\Response(
     *         response=400,
     *         description="Something went wrong"
     *      ),
     *   )
     */
    public function show(User $user)
    {
        Gate::authorize('view', $user);

        try {
            $this->throwModelNotFoundException($user);

            return new UserResource($user);
        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'success' => false,
            ], ($e instanceof ModelException) ? $e->getStatusCode() : Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update a old resource in storage.
     *
     * @OA\PUT(
     *       path="/api/v1/auth/admin/users/{id}",
     *       operationId="updateUser",
     *       tags={"Users"},
     *       summary="Update User",
     *       description="Update User",
     *       security={
     *           {"sanctumToken": {}}
     *       },
     *       @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The ID of the User to update",
     *         required=true,
     *
     *         @OA\Schema(
     *           example=1,
     *           type="number"
     *         )
     *       ),
     *        @OA\RequestBody(
     *            required=true,
     *            @OA\JsonContent(ref="#/components/schemas/UserInput"),
     *              @OA\MediaType(
     *                mediaType="multipart/form-data",
     *
     *                @OA\Schema(
     *                    type="object",
     *                    ref="#/components/schemas/UserInput")
     *            )
     *       ),
     *
     *       @OA\Response(
     *           response=200,
     *           description="Success",
     *
     *          @OA\JsonContent(ref="#/components/schemas/UserOutput")
     *       ),
     *
     *       @OA\Response(
     *           response=400,
     *           description="Bad request"
     *       ),
     *       @OA\Response(
     *           response=401,
     *           description="Unauthorized"
     *       ),
     *       @OA\Response(
     *           response=403,
     *           description="Forbidden"
     *       ),
     *       @OA\Response(
     *           response=404,
     *           description="Not found"
     *       ),
     *       @OA\Response(
     *           response=422,
     *           description="Unprocessable Entity"
     *       )
     *  )
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        try {
            $this->throwModelNotFoundException($user);
            $user = $this->userService->setUser($user, $request, false);

            return (new UserResource($user))->response()->setStatusCode(Response::HTTP_ACCEPTED);
        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'success' => false,
            ], ($e instanceof ModelException) ? $e->getStatusCode() : Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Delete a resource in storage.
     *
     * @OA\DELETE(
     *       path="/api/v1/auth/admin/users/{id}",
     *       operationId="deleteUser",
     *       tags={"Users"},
     *       summary="Delete User",
     *       description="Delete User",
     *       security={
     *           {"sanctumToken": {}}
     *       },
     *       @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The ID of the User to delete",
     *         required=true,
     *
     *         @OA\Schema(
     *           example=1,
     *           type="number"
     *         )
     *       ),
     *
     *       @OA\Response(
     *           response=200,
     *           description="Success",
     *       ),
     *
     *       @OA\Response(
     *           response=400,
     *           description="Bad request"
     *       ),
     *       @OA\Response(
     *           response=401,
     *           description="Unauthorized"
     *       ),
     *       @OA\Response(
     *           response=403,
     *           description="Forbidden"
     *       ),
     *       @OA\Response(
     *           response=404,
     *           description="Not found"
     *       ),
     *       @OA\Response(
     *           response=422,
     *           description="Unprocessable Entity"
     *       )
     *  )
     */
    public function destroy(User $user)
    {
        Gate::authorize('view', $user);
        try {
            $this->throwModelNotFoundException($user);
            $this->userService->deleteUser($user);
            return response(null, Response::HTTP_NO_CONTENT);
        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'success' => false,
            ], ($e instanceof ModelException) ? $e->getStatusCode() : Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * lock a user.
     *
     * @OA\POST(
     *       path="/api/v1/auth/admin/users/{id}/lock",
     *       operationId="lockUser",
     *       tags={"Users"},
     *       summary="lock User",
     *       description="lock User",
     *       security={
     *           {"sanctumToken": {}}
     *       },
     *       @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The ID of the User to lock",
     *         required=true,
     *
     *         @OA\Schema(
     *           example=1,
     *           type="number"
     *         )
     *       ),
     *
     *       @OA\Response(
     *           response=200,
     *           description="Success",
     *          @OA\JsonContent(ref="#/components/schemas/UserOutput")
     *       ),
     *
     *       @OA\Response(
     *           response=400,
     *           description="Bad request"
     *       ),
     *       @OA\Response(
     *           response=401,
     *           description="Unauthorized"
     *       ),
     *       @OA\Response(
     *           response=403,
     *           description="Forbidden"
     *       ),
     *       @OA\Response(
     *           response=404,
     *           description="Not found"
     *       ),
     *       @OA\Response(
     *           response=422,
     *           description="Unprocessable Entity"
     *       )
     *  )
     */
    public function lock(Request $request, User $user)
    {
        Gate::authorize('lock', $user);

        try {
            $this->throwModelNotFoundException($user);
            $user = $this->userService->lock($user, $request);
            return new UserResource($user);
        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'success' => false,
            ], ($e instanceof ModelException) ? $e->getStatusCode() : Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Reset the password.
     *
     * @OA\POST(
     *       path="/api/v1/auth/admin/users/{id}/resetPassword",
     *       operationId="resetPassword",
     *       tags={"Users"},
     *       summary="resetPassword",
     *       description="resetPassword",
     *       security={
     *           {"sanctumToken": {}}
     *       },
     *       @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The ID of the User to reset password",
     *         required=true,
     *
     *         @OA\Schema(
     *           example=1,
     *           type="number"
     *         )
     *       ),
    *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                      type="object",
     *                      @OA\Property(
     *                          property="newPassword",
     *                          type="string"
     *                      ),
     *                      @OA\Property(
     *                          property="oldPassword",
     *                          type="string"
     *                      ),
     *                      @OA\Property(
     *                          property="confirmPassword",
     *                          type="string"
     *                      )
     *                 ),
     *                 example={
     *                     "oldPassword":"123456789",
     *                     "newPassword":"123456789012",
     *                     "confirmPassword":"123456789012"
     *                }
     *             )
     *         )
     *      ),
     *       @OA\Response(
     *           response=200,
     *           description="Success",
     *          @OA\JsonContent(
     *                  @OA\Property(property="success", type="boolean", example="true"),
     *                  @OA\Property(property="message", type="string", example="Mot de passe modifié avec succés"),
     *          )
     *       ),
     *
     *       @OA\Response(
     *           response=400,
     *           description="Bad request"
     *       ),
     *       @OA\Response(
     *           response=401,
     *           description="Unauthorized"
     *       ),
     *       @OA\Response(
     *           response=403,
     *           description="Forbidden"
     *       ),
     *       @OA\Response(
     *           response=404,
     *           description="Not found"
     *       ),
     *       @OA\Response(
     *           response=422,
     *           description="Unprocessable Entity"
     *       )
     *  )
     */
    public function resetPassword(Request $request, User $user)
    {
        Gate::authorize('update', $user);
        try {
            $this->throwModelNotFoundException($user);
            $result = $this->userService->resetPassword($user, $request);

            return response()->json([
                'succes' => $result['sucess'],
                'message' => $result['message']
            ]);
        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'success' => false,
            ], ($e instanceof ModelException) ? $e->getStatusCode() : Response::HTTP_BAD_REQUEST);
        }
    }
}
