<?php

namespace App\Http\Controllers;

use Throwable;
use App\Exceptions\DomainException;
use App\Exceptions\ModelException;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Info(
 *     x={
 *          "logo": {
 *              "url": "https://via.placeholder.com/190x90.png?text=L5-Swagger"
 *          }
 *      },
 *    title="Printing API",
 *    description="An API to manage documents",
 *    version="1.0.0",
 *    @OA\Contact(
 *      email="contact@initialde.com"
 *     )
 * ),
 *  
 *  @OA\Server(
 *      url=L5_SWAGGER_CONST_HOST,
 *      description="Local Server"
 *  ),
 * 
 *  @OA\SecurityScheme(
 *     type="http",
 *     bearerFormat="JWT",
 *     scheme="Bearer",
 *     in="header",
 *     securityScheme="sanctumToken",
 *     name="Authorization",
 * )
 */
abstract class Controller
{
    protected function followExceptionMessage(Throwable $throwable): string
    {
        $standardMessage = 'Error #'.$throwable->getCode();
        $message = $standardMessage;

        if ($throwable instanceof DomainException) {
            $message = $throwable->getMessage();
        }

        return $message;
    }

    /**
     * call only in find, delete and update method
     *
     * @throws DomainException
     */
    protected function throwModelNotFoundException(Model $model): void
    {
        if (empty($model->id)) {
            throw ModelException::throwException(ModelException::ModelNotFoundException, ['resource' => $model]);
        }
    }
}
