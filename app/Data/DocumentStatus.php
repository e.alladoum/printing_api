<?php

namespace App\Data;

class DocumentStatus extends Data
{
    const PRINTED = 'printed';

    const UNPRINTED = 'unprinted';

    const PENDING = 'pending';

    const list = [
        [
            'id' => self::PRINTED,
            'label' => 'Imprimé',
        ],
        [
            'id' => self::UNPRINTED,
            'label' => 'Non imprimé',
        ],
        [
            'id' => self::PENDING,
            'label' => 'En attente',
        ],
    ];

    public function __construct()
    {
        parent::__construct();
        $this->list = self::list;
    }
}
