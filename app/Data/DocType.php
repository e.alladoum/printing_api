<?php

namespace App\Data;

class DocType extends Data
{
    const AUTHORIZATION = "AUTHORIZATION";
    const LICENSE = "LICENSE";
    const PERMIT = "PERMIT";
    const CAR_REGISTRATION = "CAR_REGISTRATION";
    const UNCATEGORIZED = "UNCATEGORIZED";

    const list = [
        [
            'id' => self::AUTHORIZATION,
            'label' => 'Autorisation',
        ],
        [
            'id' => self::CAR_REGISTRATION,
            'label' => 'Carte grise',
        ],
        [
            'id' => self::LICENSE,
            'label' => 'Licence',
        ],
        [
            'id' => self::PERMIT,
            'label' => 'Permis',
        ],
        [
            'id' => self::UNCATEGORIZED,
            'label' => 'Non classé',
        ],
    ];

    public function __construct()
    {
        parent::__construct();
        $this->list = self::list;
    }
}
