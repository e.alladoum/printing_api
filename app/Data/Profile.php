<?php

namespace App\Data;

class Profile extends Data
{

    const ADMIN = 'ADMINISTRATOR';
    const AGENT = 'AGENT';


    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_AGENT = 'ROLE_AGENT';


    const KEY_ADMIN = 'admin';
    const KEY_AGENT = 'agent';


    const LIST = [
        [
            'id' => self::ADMIN,
            'label' => 'Administrateur',
            'role' => self::ROLE_ADMIN,
            'key' => self::KEY_ADMIN,
        ],
        [
            'id' => self::AGENT,
            'label' => 'Fonctionnaire',
            'role' => self::ROLE_AGENT,
            'key' => self::KEY_AGENT,
        ],
    ];

    public function __construct()
    {
        parent::__construct();
        $this->list = self::LIST;
    }

    public static function getRoles(string $profileCode): array
    {
        return [self::getItem($profileCode)['role']];
    }

    public static function getKey(string $profileCode): ?string
    {
        if ($item = self::getItem($profileCode)) {
            return $item['key'];
        }

        return null;
    }
}
