<?php

namespace App\Models;

use App\Models\Scopes\OrderByUpdatedAtScope;
use Illuminate\Database\Eloquent\Attributes\ScopedBy;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @OA\Schema(
 *     schema="DocumentTypeOutput",
 *     title="DocumentTypeOutput",
 *     properties={
 *
 *         @OA\Property(property="id", type="number"),
 *         @OA\Property(property="name", type="string"),
 *         @OA\Property(property="description", type="string"),
 *         @OA\Property(property="configuration", type="string"),
 *         @OA\Property(property="created_at", type="date"),
 *         @OA\Property(property="updated_at", type="date"),
 *     }
 * )
 *
 * @OA\Schema(
 *     schema="DocumentTypeInput",
 *     title="DocumentTypeInput",
 *     properties={
 *
 *         @OA\Property(property="name", type="string"),
 *         @OA\Property(property="description", type="string"),
 *         @OA\Property(property="configuration", type="string"),
 *     }
 * )
 */
#[ScopedBy([OrderByUpdatedAtScope::class])]
class DocumentType extends Model
{
    use HasFactory;

    protected $hidden = ['documents'];
    
    public function documents(): HasMany
    {
        return $this->hasMany(Document::class);
    }
}
