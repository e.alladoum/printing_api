<?php

namespace App\Models;

use App\Models\Scopes\OrderByUpdatedAtScope;
use Illuminate\Database\Eloquent\Attributes\ScopedBy;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @OA\Schema(
 *     schema="BatchOutput",
 *     title="BatchOutput",
 *     properties={
 *
 *         @OA\Property(property="id", type="number"),
 *         @OA\Property(property="number", type="number"),
 *         @OA\Property(property="label", type="string"),
 *         @OA\Property(property="begin", type="date"),
 *         @OA\Property(property="end", type="date"),
 *         @OA\Property(property="documents", type="array", @OA\Items(ref="#/components/schemas/DocumentOuput")),
 *         @OA\Property(property="created_at", type="date"),
 *         @OA\Property(property="updated_at", type="date"),
 *     }
 * )
 *
 * @OA\Schema(
 *     schema="BatchInput",
 *     title="BatchInput",
 *     properties={
 *
 *         @OA\Property(property="label", type="string"),
 *         @OA\Property(property="number", type="number"),
 *         @OA\Property(property="begin", type="date"),
 *         @OA\Property(property="end", type="date"),
 *         @OA\Property(property="documents", type="array", @OA\Items(type="number")),
 *     }
 * )
 */
#[ScopedBy(OrderByUpdatedAtScope::class)]
class Batch extends Model
{
    use HasFactory, SoftDeletes;

    public function documents(): HasMany
    {
        return $this->hasMany(Document::class);
    }

    public function scopeWithDocuments(Builder $builder)
    {
        $builder->with('documents');
    }
}
