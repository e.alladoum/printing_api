<?php

namespace App\Models\Traits;

use App\Classes\ModelChangesDescription;
use App\Classes\Trace;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

trait Traceable
{
    /**
     * Will save model with trace
     */
    public function saveWithTrace(array $options = []): bool|null
    {
        /**@var User $connectedUser */
        $connectedUser = Auth::user();

        if (!empty($connectedUser)) {
            $stacktrace = json_decode($this->stacktrace) ?? [];

            if (!isset($options['action'])) {
                $action = 'create';
                if (!empty($this->id)) {
                    $action = 'update';
                }
            } else {
                $action = $options['action'];
                unset($options['action']);
            }
            $stacktrace[] = (new Trace(
                $connectedUser,
                $action,
                new ModelChangesDescription($this),
                "printing_module"
            ))->toArray();

            $this->stacktrace = json_encode($stacktrace);

            return parent::save($options);
        }

        return false;
    }

    public function delete(): bool|null
    {
        if (parent::delete()) {
            $this->saveWithTrace(['action' => 'delete']);
            return true;
        }

        return null;
    }
}
