<?php

namespace App\Models;

use App\Data\DocumentStatus;
use App\Models\Scopes\OrderByUpdatedAtScope;
use App\Models\Traits\HasFiles;
use App\Models\Traits\Traceable;
use App\Models\Traits\Uuid;
use Illuminate\Database\Eloquent\Attributes\ScopedBy;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * @OA\Schema(
 *     schema="DocumentOuput",
 *     title="DocumentOuput",
 *     properties={
 *
 *         @OA\Property(property="id", type="number"),
 *         @OA\Property(property="number", type="number"),
 *         @OA\Property(property="document_type_id", type="number"),
 *         @OA\Property(property="status", type="string"),
 *         @OA\Property(property="uuid", type="string"),
 *         @OA\Property(property="status_label", type="string"),
 *         @OA\Property(property="stacktrace", type="string"),
 *         @OA\Property(property="batch_id", type="number"),
 *         @OA\Property(property="batch", type="object"),
 *         @OA\Property(property="document_type", type="object"),
 *         @OA\Property(property="files", type="array", @OA\Items(type="string")),
 *         @OA\Property(property="created_at", type="date"),
 *         @OA\Property(property="updated_at", type="date"),
 *         @OA\Property(property="deleted_at", type="date"),
 *     }
 * )
 *
 * @OA\Schema(
 *     schema="DocumentInput",
 *     title="DocumentInput",
 *     properties={
 *
 *         @OA\Property(property="number", type="number"),
 *         @OA\Property(property="document_type_id", type="number"),
 *         @OA\Property(property="status", type="string"),
 *         @OA\Property(property="batch_id", type="number"),
 *         @OA\Property(property="files", type="array", @OA\Items(type="string")),
 *     }
 * )
 */
#[ScopedBy([OrderByUpdatedAtScope::class])]
class Document extends Model
{
    use HasFactory;
    use SoftDeletes;
    use HasFiles;
    use Traceable;
    use Uuid;

    protected $casts = [
        'files' => 'array',
    ];



    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['status_label'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['status', 'batch_id'];

    protected function statusLabel(): Attribute
    {
        return Attribute::make(
            get: fn (mixed $value, array $attributes) => DocumentStatus::getLabel($attributes['status']),
        );
    }

    public function documentType(): BelongsTo
    {
        return $this->belongsTo(DocumentType::class);
    }

    public function batch(): BelongsTo
    {
        return $this->belongsTo(Batch::class);
    }


    /**
     * All scopes
     */


    public function scopeWithDocumentType(Builder $builder)
    {
        $builder->with('documentType');
    }
    public function scopeWithBatch(Builder $builder)
    {
        $builder->with('batch');
    }

    public function scopeByBatchId(Builder $builder, int $batch_id)
    {
        $builder->where('batch_id', $batch_id);
    }

    public function scopeByDocumentTypeId(Builder $builder, int $document_type_id)
    {
        $builder->where('document_type_id', $document_type_id);
    }

    public function scopeByStatus(Builder $builder, string $status)
    {
        $builder->where('status', $status);
    }
}
