<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use App\Data\Profile;
use App\Models\Scopes\OrderByUpdatedAtScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Traits\HasFiles;
use Illuminate\Database\Eloquent\Attributes\ScopedBy;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Sanctum\HasApiTokens;

/**
 * @OA\Schema(
 *     schema="UserOutput",
 *     title="UserOutput",
 *     properties={
 *
 *         @OA\Property(property="id", type="number"),
 *         @OA\Property(property="profile", type="string"),
 *         @OA\Property(property="registration", type="string"),
 *         @OA\Property(property="email", type="string"),
 *         @OA\Property(property="name", type="string"),
 *         @OA\Property(property="firstname", type="string"),
 *         @OA\Property(property="profile_label", type="string"),
 *         @OA\Property(property="password", type="string"),
 *         @OA\Property(property="image", type="string"),
 *         @OA\Property(property="email_verified_at", type="datetime"),
 *         @OA\Property(property="is_locked", type="boolean"),
 *         @OA\Property(property="has_password_temp", type="boolean"),
 *         @OA\Property(property="created_at", type="date"),
 *         @OA\Property(property="updated_at", type="date"),
 *         @OA\Property(property="deleted_at", type="date"),
 *     }
 * )
 *
 * @OA\Schema(
 *     schema="UserInput",
 *     title="UserInput",
 *     properties={
 *
 *         @OA\Property(property="profile", type="string"),
 *         @OA\Property(property="registration", type="string"),
 *         @OA\Property(property="email", type="string"),
 *         @OA\Property(property="name", type="string"),
 *         @OA\Property(property="firstname", type="string"),
 *         @OA\Property(property="image", type="file"),
 *         @OA\Property(property="email_verified_at", type="string"),
 *         @OA\Property(property="password", type="string"),
 *         @OA\Property(property="is_locked", type="boolean"),
 *         @OA\Property(property="has_password_temp", type="boolean"),
 *     }
 * )
 */

#[ScopedBy([OrderByUpdatedAtScope::class])]
class User extends Authenticatable
{
    use HasFactory, Notifiable, HasFiles ,HasApiTokens, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'remember_token',
    ];

        /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['profile_label'];

    /**
     * Get the attributes that should be cast.
     *
     * @return array<string, string>
     */
    protected function casts(): array
    {
        return [
            'email_verified_at' => 'datetime',
            'password' => 'hashed',
        ];
    }

    protected function profileLabel(): Attribute
    {
        return Attribute::make(
            get: fn (mixed $value, array $attributes) => Profile::getLabel($attributes['profile']),
        );
    }
}
