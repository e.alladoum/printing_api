<?php

namespace App\Services\Domain;

use App\Data\DocumentStatus;
use App\Data\FileType;
use App\Http\Requests\Document\StoreDocumentRequest;
use App\Http\Requests\Document\UpdateDocumentRequest;
use App\Models\Document;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class DocumentService extends Service
{
    public function getQuery(Request $request): Builder
    {
        $queryBuilder = Document::query();

        $queryBuilder->when(
            $request->has('last_update'),
            fn (Builder $query) => $query->whereDate('updated_at', '>', Carbon::parse($request->last_update))
        );
        $queryBuilder->when(
            $request->has('document_type'),
            fn (Builder $query) => $query->withDocumentType()
        );

        $queryBuilder->when(
            $request->has('batch'),
            fn (Builder $query) => $query->withBatch()
        );
        $queryBuilder->when(
            $request->has('batch_id'),
            fn (Builder $query) => $query->byBatchId($request->input('batch_id'))
        );
        
        $queryBuilder->when(
            $request->has('document_type_id'),
            fn (Builder $query) => $query->byDocumentTypeId($request->input('document_type_id'))
        );

        $queryBuilder->when(
            $request->has('status'),
            fn (Builder $query) => $query->byStatus($request->input('status'))
        );

        return $queryBuilder;
    }

    public function setDocument(Document $document, StoreDocumentRequest|UpdateDocumentRequest $request): Model
    {
        $document = $this->set($document, $request, function (Document &$document) use ($request) {
            if ($files = $request->file('files')) {
                $document->addFiles($files, FileType::DOCUMENT_FILES);
            }
        });

        $document->saveWithTrace();
        return $document;
    }

    public function deleteDocument(Document $document)
    {
        $this->delete($document);
    }

    public function changeDocumentStatus(Document $document, Request $request): Model
    {
        $document->status = $request->input('state', DocumentStatus::PENDING);
        $document->save();
        return $document;
    }
}
