<?php

namespace App\Services\Domain;

use App\Data\DocumentStatus;
use App\Http\Requests\DocumentType\StoreDocumentTypeRequest;
use App\Http\Requests\DocumentType\UpdateDocumentTypeRequest;
use App\Models\DocumentType;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class DocumentTypeService extends Service
{
    public function getQuery(Request $request): Builder
    {
        $queryBuilder = DocumentType::query();

        $queryBuilder->when(
            $request->has('last_update'),
            fn (Builder $query) => $query->whereDate('updated_at', '>', Carbon::parse($request->last_update))
        );

        return $queryBuilder;
    }

    public function setDocumentType(DocumentType $documentType, StoreDocumentTypeRequest|UpdateDocumentTypeRequest $request): Model
    {
        return $this->set($documentType, $request);
    }

    public function deleteDocumentType(DocumentType $documentType)
    {
        $this->delete($documentType);
    }

    public function count(Request $request)
    {
        $result = null;

        $query = DocumentType::with('documents')->withCount(['documents as documents_all', 'documents as documents_printed' => function (Builder $query) {
            $query->where('status', DocumentStatus::PRINTED);
        }, 'documents as documents_pending' => function (Builder $query) {
            $query->where('status', DocumentStatus::PENDING);
        }]);

        if ($request->has('document_type_name')) {
            $result = $query->where('name', $request->input('document_type_name'))->first();
        } else
            $result = $query->get();

        return $result;
    }
}
