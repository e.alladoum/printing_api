<?php

namespace App\Services\Domain;

use App\Data\DocumentStatus;
use App\Http\Requests\Batch\StoreBatchRequest;
use App\Http\Requests\Batch\UpdateBatchRequest;
use App\Models\Batch;
use App\Models\Document;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class BatchService extends Service
{
    public function getQuery(Request $request): Builder
    {
        $queryBuilder = Batch::query();

        $queryBuilder->when(
            $request->has('last_update'),
            fn (Builder $query) => $query->whereDate('updated_at', '>', Carbon::parse($request->last_update))
        );
        $queryBuilder->when(
            $request->has('documents'),
            fn (Builder $query) => $query->withDocuments()
        );

        return $queryBuilder;
    }

    public function deleteBatch(Batch $batch)
    {
        $this->delete($batch);
    }

    public function setBatch(Batch $batch, StoreBatchRequest|UpdateBatchRequest $request): Model
    {
        return  $this->set($batch, $request, function (Batch &$batch) use ($request) {
            if ($request->has('documents')) {
                $batch->save();
                Document::whereIn('id', $request->input('documents'))->update([
                    'status' => DocumentStatus::PRINTED,
                    'batch_id' => $batch->id
                ]);
            }
        },['documents']);
    }
}
