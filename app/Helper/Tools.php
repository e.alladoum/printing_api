<?php

namespace App\Helper;

use App\Classes\Constant;
use App\Data\FileType;

abstract class Tools
{
    public static function buildFileName(string $fileType, string $fileExtension = Constant::PNG): string
    {
        return FileType::getLabel($fileType).'_'.uniqid().'.'.$fileExtension;
    }
}
