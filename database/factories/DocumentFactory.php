<?php

namespace Database\Factories;

use App\Models\Batch;
use App\Models\DocumentType;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Document>
 */
class DocumentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'number' => fake()->randomNumber,
            'document_type_id' => fake()->numberBetween(1, DocumentType::count()),
            'batch_id' => fake()->numberBetween(1, Batch::count()),
        ];
    }
}
