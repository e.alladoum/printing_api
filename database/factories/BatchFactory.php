<?php

namespace Database\Factories;

use DateInterval;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Batch>
 */
class BatchFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $begin = fake()->dateTime;
        $end = clone $begin;
        $end->add(new DateInterval('P1W'));

        return [
            'label' => fake()->currencyCode(),
            'number' => fake()->randomDigit(),
            'begin' => $begin,
            'end' => $end
        ];
    }
}
