<?php

namespace Database\Seeders;

use App\Data\Profile;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::factory()->create([
            'registration' => '12345',
            'profile' => Profile::ADMIN,
        ]);

        User::factory()->create([
            'registration' => '56978',
            'profile' => Profile::AGENT,
        ]);
    }
}
