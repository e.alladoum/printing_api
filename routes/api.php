<?php

use App\Http\Controllers\Api\BatchController;
use App\Http\Controllers\Api\DocumentController;
use App\Http\Controllers\Api\DocumentTypeController;
use App\Http\Controllers\Api\SecurityController;
use App\Http\Controllers\Api\UserController;
use App\Http\Middleware\AlwaysAcceptsJson;
use Illuminate\Support\Facades\Route;

Route::post('/login', [SecurityController::class, 'login']);


Route::middleware(['auth:sanctum', 'throttle:api' , AlwaysAcceptsJson::class])->prefix('auth')->group(function() {
    Route::post('/logout', [SecurityController::class, 'logout']);

    Route::middleware('can:isAdministrator')->prefix('admin')->group(function() {
        Route::apiResource('document_types',DocumentTypeController::class);
        Route::get('document_types/documents/count',[DocumentTypeController::class, 'count']);

        Route::apiResource('users',UserController::class);
        Route::post('users/{user}/lock', [UserController::class, 'lock']);
        Route::post('users/{user}/resetPassword', [UserController::class, 'resetPassword']);
    });

    
    Route::apiResource('documents', DocumentController::class);
    Route::post('documents/{document}/changeStatus', [DocumentController::class, 'changeStatus']);

    Route::apiResource('batches', BatchController::class);
});
